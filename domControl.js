console.log("domControlStart");
//CONSTRAINT FUNCTIONS
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

//RECOGNITION VARIABLE
var recognition = new webkitSpeechRecognition();

//ONSTART
recognition.onstart = function() {
    sleep(8000) 
    console.log('Voice recognition activated. Try speaking into the microphone.');
}

//ON RECOGNITION END
recognition.onend = function() {
    
    console.log('You were quiet for a while so voice recognition turned itself off.');
    recognition.start()
}

recognition.onresult = function(event) { 
    var command = event.results[0][0].transcript;
    command = command.split(" ");
    //TAKING THE COMMAND FROM USER
    console.log(command);
    var initial_command = command[0];
    command[0] = command[0].toLowerCase()
    var port = chrome.runtime.connect({name: "command"});
    switch (initial_command)
    {
        //OPEN 
        case 'open':
        {
            switch(command[1])
            {
                //OPEN NEW TAB
                case 'new':
                {
                    port.postMessage({"commands": "Open new tab"});
                    break;
                }
                default :
                {
                    port.postMessage({"commands":"new " + command[1] })
                    break;
                }
            }
            break;
        }
        

        
        //SEARCH FEATURE
        case 'search':
        {
            var output = document.getElementsByTagName('input');
            
            for (elem in output){
                var search_field;
                var nodeElem = output[elem]
                
                if(nodeElem.hasAttribute('placeholder')){
                    if(nodeElem.getAttribute('placeholder').toLowerCase().includes('search')){
                        search_field = output[elem]
                        break;
                    }
                }
                else search_field = 'None'
            }
            console.log(search_field);
            search_field.value = command[1]
            // var search_btn = document.getElementById('search-icon-legacy');
            // var command_operator_1 = command[1]
            
            // command.splice(0,1);
            // console.log(command);
            // output[0].value = command.join(" ");
            // search_btn.click();
            break;
        }
        //PLAY FEATURE
        case 'play':
        {
            var video_links_elements = document.getElementsByClassName('yt-simple-endpoint inline-block style-scope ytd-thumbnail')
            var video_links = [];
            Array.from(video_links_elements).forEach(element => {
                var link = element.getAttribute('href');
                video_links.push("https://www.youtube.com"+link)
            });
            console.log(video_links);
            var video_number = command[1].toLowerCase();
            switch (video_number)
            {
                case 'one':
                case 'first':
                {
                    window.location.href = video_links[0];
                    break;
                }
                case 'two':
                case 'second':
                {
                    window.location.href = video_links[1];
                    break;
                }
                default:
                console.log('Only the first 22 searches are allowed.Link not available');
            }
            break;
        }
        //VIDEO PLAYER CONTROLS
        case 'stop':
        case 'resume':
        {
            document.getElementsByClassName('ytp-play-button')[0].click();
            break;
        }
        case 'next':
        {
            document.getElementsByClassName('ytp-next-button')[0].click();
            break;
        }
        case 'mute':
        case 'unmute':
        {
            document.getElementsByClassName('ytp-mute-button')[0].click();
            break;
        }
        default:
        {
            console.log('No such voice command exist');
        }
        
        
    }
}
recognition.start();
// setInterval(() => recognition.start(),4000);


