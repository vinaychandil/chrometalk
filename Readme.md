# ChromeTalk

ChromeTalk is a chrome extension that utilizes voice recognition and browser/website interaction to enable voice commands on popular websites like Youtube. It can be scaled to any website and also provides comprehensive documentation.

## Overview

ChromeTalk is a chrome extension designed to make the browser voice-controlled. It offers special features to control popular websites like Youtube, Facebook, and more using voice recognition. The extension is built with the idea to serve physically challenged individuals, enhancing their browsing experience through voice commands.

## Technology Stack

The extension is built using Python, JavaScript, REST APIs, and MongoDB.

## Active Voice Commands

### General Commands

- **Open new tab**: Opens up a new tab on your chrome browser.
- **Open website_name**: Opens up the said website in a new tab.
- **Search this**: Will search in the search bar of the active tab.

### Youtube Specific Commands

- **Play Number**: Opens up the specified result on a Youtube search.
- **Stop**: Pauses the currently playing video.
- **Resume**: Resumes the paused video.
- **Mute**: Mutes the video.
- **Unmute**: Unmutes the video.
- **Next**: Plays the next available video.

## Usage

To use ChromeTalk, simply install the extension and enable the voice recognition feature. Once activated, you can start using the voice commands to control your browsing experience.

## Documentation

Comprehensive documentation is available to guide users on how to install, configure, and use ChromeTalk. It also provides insights into the technology stack and the underlying implementation.

## Contribution

Contributions to ChromeTalk are welcome. Feel free to fork the repository, make improvements, and submit a pull request.
